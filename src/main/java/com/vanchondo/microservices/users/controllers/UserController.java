package com.vanchondo.microservices.users.controllers;

import java.util.List;

import javax.security.sasl.AuthenticationException;
import javax.websocket.server.PathParam;

import com.vanchondo.vventas.entities.User;
import com.vanchondo.vventas.jwt.CurrentUser;
import com.vanchondo.vventas.jwt.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vanchondo.microservices.users.services.AuthenticationService;
import com.vanchondo.microservices.users.services.UserService;

@RestController
public class UserController {

    private UserService userService;
    private AuthenticationService authenticationService;

    @GetMapping("")
    public List<User> allUsers(){
        return userService.findAll();
    }

    @PostMapping("login")
    public Token login(@RequestBody User user) throws AuthenticationException {
        return authenticationService.login(user);
    }

    @DeleteMapping("")
    public void deleteUserByEmail(@RequestAttribute("currentUser") CurrentUser currentUser, @PathParam("email") String email){
        userService.deleteUserByEmail(currentUser, email);
    }

    @PostMapping("/")
    public User save(@RequestBody User user){
        return userService.save(user);
    }

    @PutMapping("")
    public User update(@RequestAttribute("currentUser") CurrentUser currentUser, @RequestBody User user){
        return userService.update(currentUser, user);
    }

    @GetMapping("initializeDB")
    public List<User> initializeDB(){
        return userService.initializeDB();
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
