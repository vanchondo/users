package com.vanchondo.microservices.users.services;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.vanchondo.vventas.entities.RoleEnum;
import com.vanchondo.vventas.entities.Store;
import com.vanchondo.vventas.entities.User;
import com.vanchondo.vventas.jwt.CurrentUser;
import com.vanchondo.vventas.utilities.UserUtility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vanchondo.microservices.users.repositories.UserRepository;

@Service
public class UserService {
    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;
    private AuthenticationService authenticationService;
    private StoreService storeService;

    public User findByEmail(String email){
        logger.debug("Searching by email: "+ email);
        //Avoid searching using special characters or non-email format
        if (!UserUtility.isEmailValid(email)) {
            throw new IllegalArgumentException("Email not valid");
        }
        else{
            User user = userRepository.findByEmail(email);  //Returns null when no records are found
            logger.debug("Found: " + user);
            return user;
        }
    }

    public User save(User user){
        logger.debug("Trying to save: "+ user);

        if (StringUtils.isEmpty(user.getEmail())){
            logger.debug("Invalid email or store");
            throw new IllegalArgumentException("Invalid email or store");
        }
        if (StringUtils.isEmpty(user.getStore())){
            user.setStore(user.getEmail());
        }

        if (findByEmail(user.getEmail()) != null) {
            logger.debug("Email already in use");
            throw new IllegalArgumentException("Email already in use");
        }
        else if (storeService.findByName(user.getStore()) != null){
            logger.debug("Store already in use");
            throw new IllegalArgumentException("Store already in use");
        }
        else
        {
            user.setPassword(authenticationService.getEncodedPassword(user.getPassword()));
            user.setUpdatedAt(new Date());
            user.setUpdatedBy("web");
            user.setDeleted(false);
            storeService.save(new Store(user.getStore()));
            User userDB = userRepository.save(user);
            logger.debug("User saved: "+ userDB);
            return userDB;
        }
    }

    public User update(CurrentUser currentUser, User user){
        logger.debug("Trying to update: "+ user);
        if (!currentUser.getEmail().equals(user.getEmail()) && !UserUtility.isAdmin(currentUser.getRoles())){
            logger.debug("Invalid attempt to update user: "+ user + " by the user: " + currentUser.getEmail());
            throw new IllegalArgumentException("Invalid attempt to update user");
        }
        User userDB = findByEmail(user.getEmail());
        if (userDB == null) {
            logger.debug("User "+user.getEmail()+" does not exist");
            throw new IllegalArgumentException("User does not exist");
        }
        else
        {
            userDB.setPassword(authenticationService.getEncodedPassword(user.getPassword()));
            userDB.setRoles(user.getRoles());
            userDB.setUpdatedAt(new Date());
            userDB.setUpdatedBy(currentUser.getEmail());
            userDB.setDeleted(false);
            userDB = userRepository.save(userDB);
            logger.debug("User updated: "+ userDB);
            return userDB;
        }
    }

    public void deleteUserByEmail(CurrentUser currentUser, String email){
        logger.debug("Trying to delete: " + email);
        if (!currentUser.getEmail().equals(email) && !UserUtility.isAdmin(currentUser.getRoles())){
            logger.debug("Invalid attempt to delete email: "+ email + " by the user: " + currentUser.getEmail());
            throw new IllegalArgumentException("Invalid attempt to delete email");
        }
        User userDB = findByEmail(email);
        if (userDB == null){
            logger.debug("Email not found");
            throw new IllegalArgumentException("Email not found");
        }
        else{
            userDB.setUpdatedBy(currentUser.getEmail());
            userDB.setUpdatedAt(new Date());
            userDB.setDeleted(true);
            userDB = userRepository.save(userDB);
            if (userDB.isDeleted()){
                logger.debug("User set as deleted with email "+ email);
            }
            else{
                logger.debug("User not deleted with email "+ email);
                throw new IllegalArgumentException("User not deleted.");
            }
        }
    }

    public List<User> findAll(){
        logger.debug("Retrieving all users");
        List<User> users = userRepository.findAll();
        logger.debug("Users retrieved: "+users.stream().map(Object::toString).collect(Collectors.joining(",")));
        return users;
    }

    public List<User> initializeDB(){
        logger.debug("Initializing users DB");
        logger.debug("Deleting all users");
        userRepository.deleteAll();
        storeService.initializeDB();

        User user = new User();
        user.setEmail("eduardoanchondo@gmail.com");
        user.setPassword("pass");
        user.getRoles().add(RoleEnum.ADMIN.toString());
        save(user);

        user = new User();
        user.setEmail("victoranchondo@gmail.com");
        user.setPassword("pass");
        user.getRoles().add(RoleEnum.USER.toString());
        save(user);

        return findAll();
    }

    @Autowired
    public void setStoreService(StoreService storeService){
        this.storeService = storeService;
    }
    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
