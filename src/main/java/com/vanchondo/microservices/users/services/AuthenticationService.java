package com.vanchondo.microservices.users.services;

import java.util.Calendar;
import java.util.Date;

import javax.security.sasl.AuthenticationException;

import com.vanchondo.vventas.entities.User;
import com.vanchondo.vventas.jwt.Token;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class AuthenticationService {
    private final static Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    private String secretKey;
    private int jwtExpirationMinutes;
    private String jwtIssuer;
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    public Token login(User userRequest) throws AuthenticationException {
        String email = userRequest.getEmail();
        String password = userRequest.getPassword();

        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)) {
            throw new IllegalArgumentException("Invalid Email or password");
        }

        User user = userService.findByEmail(email);
        if (user == null || !passwordEncoder.matches(password, user.getPassword())) {
            throw new AuthenticationException("Email or password incorrect");
        }
        if (user.isDeleted()){
            throw new AuthenticationException("User was deleted");
        }

        return generateToken(user);
    }

    public String getEncodedPassword(String password){
        return passwordEncoder.encode(password);
    }



    private Token generateToken(User currentUser){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, jwtExpirationMinutes);

        return new Token(Jwts.builder()
                .setIssuer(jwtIssuer)
                .setSubject(currentUser.getEmail())
                .claim("roles", currentUser.getRoles())
                .claim("store", currentUser.getStore())
                .setIssuedAt(new Date())
                .setExpiration(cal.getTime())
                .signWith(
                        SignatureAlgorithm.HS256,
                        secretKey)
                .compact());
    }

    @Value("${com.vanchondo.vventas.secretKey}")
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Value("${jwt.expirationToken}")
    public void setJwtExpirationMinutes(int jwtExpirationMinutes) {
        this.jwtExpirationMinutes = jwtExpirationMinutes;
    }

    @Value("${jwt.issuer}")
    public void setJwtIssuer(String jwtIssuer) {
        this.jwtIssuer = jwtIssuer;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
