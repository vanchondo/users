package com.vanchondo.microservices.users.services;

import com.vanchondo.vventas.entities.Store;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class StoreService {

    private String usersUrl;

    public Store findByName(String name){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Store> store
                = restTemplate.getForEntity(usersUrl + "?store="+name, Store.class);
        return store.getBody();
    }

    public Store save(Store store){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Store> storeReq
                = restTemplate.postForEntity(usersUrl, store, Store.class);
        return storeReq.getBody();
    }

    public void initializeDB(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(usersUrl+"initializeDB",null, Void.class);
    }

    @Value("${com.vanchondo.microservices.users.url}")
    public void setSecretKey(String usersUrl) {
        this.usersUrl = usersUrl;
    }

}
