package com.vanchondo.microservices.users.repositories;

import com.vanchondo.vventas.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface UserRepository  extends MongoRepository<User, String> {

    User findByEmail(String email);
    int deleteAllByEmail(String email);
}
